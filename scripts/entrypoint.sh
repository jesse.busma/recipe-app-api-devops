#!/bin/sh
set -e

python manage.py collectstatic --noinput
python manage.py wait_for_db
python manage.py migrate

uwsgi --socket app:9000 --workers 4 --master --enable_threads --module app.wsgi